"""
# Reformating adjacency matrices

For convenience we read the adjacency matrix format, which is the
output of Mental Modeler, and write it out as graphml for easy reading
on Cytoscape and as gpickles for easy reading on other python scripts.
"""

import csv
import networkx as nx
import numpy as np

def csv_matrix_to_digraph(path):
    """Reads an adjacency matrix in CSV format,
       returns a networkx DiGraph object"""
    reader = csv.DictReader(open(path))
    G = nx.DiGraph()

    for row in reader:
        for k in row:
            if k == '':
                continue
            if row[k] != '':
                weight=float(row[k])
                
                
                G.add_edge(row[''], k,
                            weight=weight)
                
    return G


# read in all 19 mental models and write them out as graphml and gpickle
for i in range(1, 31):
    nx.readwrite.write_graphml(
        csv_matrix_to_digraph('data_v1/%s.csv' % i),
                              'data_v1/weighted_%s.graphml' % i)
    nx.readwrite.write_gpickle(
        csv_matrix_to_digraph('data_v1/%s.csv' % i),
                              'data_v1/weighted_%s.gpickle' % i)
