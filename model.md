# Framework
Geographic space where sources and sinks are located and can be interconnected to enable the development of a CCUS project. 

## Attributes
  -	Geographic region (region where simulations occur)
  -	Rate of innovation (likelihood to develop CCUS technology)
  -	Cluster potential (existing cluster approach of willingness from industry)
  -	Area character (industry is located in dense or spread out areas)
  -	Importance of industry (is the region predominantly industrial?)
  -	Industry dependence (how relevant is the industry to sustain the social and economic development in the region)

## Behaviour
  -	Develop industrial activities
  -	Grow in population 
  -	Grow in demand for resources and services
  -	Produce goods and services
  -	Contribute to economic growth
  -	Present social conflicts related to industrial activities
  -	Have a political and social willingness for sustainable development



# Sources

A CO2 source is a stationary industrial emitter such as power generation plants, cement plant, iron & steel facilities, petrochemical plants, etc.  

## Attributes

  -	Location and distribution (latitude – longitude)
  -	Volume (tonnage of CO2 emissions produced per year)
  -	Type (direct or process-related emissions)
  -	Single or multiple vents 
  -	Space for capture systems installation 


## Behaviours

  -	Produce commodities or provide services
  -	Emit GHG emissions
  -	Produce waste
  -	Consume water
  -	Vary in size and productivity
  -	Create jobs
  -	Bring welfare and economic growth in locally
  -	Choose decarbonisation alternatives such as:
    - Energy Efficiency
    -	Cogeneration
    - Renewable Energy
    - Fuel switching 
    - CCU and CCUS
  -	Choose to develop a capture cluster with other industries to:
    -	Develop cross-industry bodies and culture
    -	Share or reuse infrastructure
    -	Reduce collective GHG emissions
    -	Reduce capital costs
    -	Reduce risks
  - Rule by public policy and market

# Sink

A CO2 sink is a geological media in the subsoil where CO2 can be injected and permanently store. For this study, oil fields suitable for miscible CO2-EOR processes are considered as sinks.

## Attributes
  -	Location (polygon and centroid – latitude and longitude)
  -	Status (producing, closed, abandoned)
  -	Recovery stage (primary, secondary, tertiary)
  -	Depth (m)
  -	Temperature (ºF)
  -	Porosity (%)
  -	Permeability (mD)
  -	Oil gravity (API)
  -	Oil viscosity (cP)
  -	Pressure (psi)
  -	Initial saturation (%)
  -	Initial pore space oil saturation
  -	Original Oil in Place (OOIP) (MMSTB)
  -	Remaining oil fraction in the reservoir (MMSSTB)

## Behaviour
  -	Produce oil
  -	Choose an EOR method for enhanced production
  -	Store CO2
  -	Connect with oil supply chain to refineries and petrochemical plants
  -	Choose to purchase and use anthropogenic CO2 for EOR operations
  -	Recycle CO2 produced from EOR operations
  -	Create jobs
  -	Create social conflict
  -	Bring economic growth to the region
  -	Consume water and energy
  -	Produce revenues from oil sell
  -	Pay royalties or taxes to governments
  - Depend on market conditions
  - Rule by public policy and national regulations

# Collection Hub

Infrastructure network created to collect and transport CO2 from sources to sinks. It include compression, liquefaction and transport systems. For this study, all the projects are onshore.

## Attributes

  -	Pressure (MPa)
  -	Temperature (ºC)
  -	Distance from source to sink
  -	Volume to be transported
  -	Routing 
  -	Compression and liquefaction
    -	Equipment cost
    -	Energy cost
    -	Operation and management
  -	Pipeline
    -	Material cost
    -	Labour cost
    -	ROW cost (payment by gatherer to landowners for rights-of-way)
    -	Length 
  -	Truck
    -	Container capacity
    -	Transport cost (distance and route-dependent)
    -	Labour cost

## Behaviour
  -	Collect, compress and transport CO2
  -	Connect sources with sinks
  -	Choose between pipelines or trucks
  -	Produce new infrastructure or reuse infrastructure
  -	Pay ROW costs
  -	Integrate capture clusters
  -	Connect fields for CO2 utilisation and storage
  -	Cross land (pipelines) or use of roads (trucks)


# Government 

## Behaviour
  - Set Public Policy : for environment protection, industry operations or social and economic development
  - Enact regulations and policies 
  - Regulate operations
  - Promote/incentive the development of decarbonisation pathways for industry
  - Recive revenues and taxes from industry and society
  - Develop infrastructure : to supply goods, provide services,and transport CO2
  - Manage the national carbon market
  

# Society

## Behaviour
  - Lives in the region where projects are developed
  - Work for the industry
  - Consume goods and services
  - Grow in size and population 
  - Demand goods and services
  - Own the land
  - Demand fair welfare conditions 
  - Pay taxes 
  - Accept or not the development of new infrastructure and projects in the region

  







