category = {'carbon price': 'economic',
            'oil price': 'economic',
            'industrial development': 'economic',
            'storage cost': 'economic',
            'transport cost': 'economic',
            'decarbonisation willingness': 'sociopolitical',
            'public acceptance': 'sociopolitical',
            'support to R&D': 'sociopolitical',
            'regulatory framework': 'sociopolitical',
            'relation with the industry': 'sociopolitical',
            'trust in institutions': 'sociopolitical',
            'MVR': 'sociopolitical',
            'demand for low-carbon product': 'sociopolitical',
            'energy consumption': 'sociopolitical',
            'liability': 'sociopolitical',
            'economic development': 'sustainability',
            'GHG emissions': 'sustainability',
            'social welfare': 'sustainability',
            'CO2 storage potential': 'technical',
            'cluster potential': 'technical',
            'decarbonisation potential': 'technical',
            'capture potential': 'technical',
            'incremental oil production': 'technical',
            'transport option': 'technical',
            'emmiters size': 'technical',
            'transport routing': 'technical',
            'source-sink distance': 'technical',
            'geological and engineering framework': 'technical',
            'clean energy': 'technical',
            'projects success cases': 'technical',
}


economic = ['carbon price',
            'oil price',
            'industrial development',
            'storage cost',
            'transport cost',
            ]

sociopolitical =['decarbonisation willingness',
                 'public acceptance',
                 'support to R&D',
                 'regulatory framework',
                 'relation with the industry',
                 'trust in institutions',
                 'MVR',
                 'demand for low-carbon product',
                 'energy consumption',
                 'liability',]


sustainability = ['economic development',
                  'GHG emissions',
                  'social welfare',]


technical = ['CO2 storage potential',
             'cluster potential',
             'decarbonisation potential',
             'capture potential',
             'incremental oil production',
             'transport option',
             'emmiters size',
             'transport routing',
             'source-sink distance',
             'geological and engineering framework',
             'clean energy',
             'projects success cases',]


c = {'sociopolitical': '#ffb96c',
     'economic': '#fcf984',
     'sustainability': '#acffd0',
     'technical': '#bdd8ff'}

