import networkx as nx
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np


# load graphs from data dir
graphs = []
for i in range(1, 31):
    g = nx.readwrite.read_gpickle('data_v1/weighted_%s.gpickle' % i)
    g.name = i
    graphs.append(g)

    
# full set of nodes
nodes = set()
for g in graphs:
    nodes = nodes.union(set(g.nodes))
nodes = list(nodes)


assert len(nodes) == 30



flow = {}
for g in graphs:
    flow[g.name] = set(
        [node + "_as_source"
         for node in nodes
         if g.in_degree(node) == 0
         and g.out_degree(node) > 0]
        +
        [node + "_as_sink"
         for node in nodes
         if g.out_degree(node) == 0
         and g.in_degree(node) > 0])



def jaccard_index(A, B):
    assert type(A) is set and type(B) is set
    if len(A.union(B)) == 0:
        return 0
    else:
        return (len(A.intersection(B))
                / 
                float(len(A.union(B))))


# now create a matrix compairing all pairs of edge sets
jin_matrix = [[jaccard_index(flow[i],
                             flow[j]) for i in flow]
               for j in flow]

# load it into a dataframe
jin_df = pd.DataFrame(jin_matrix,
                      flow.keys(),
                      flow.keys())


sns.clustermap(jin_df,
               method='complete'
              )
plt.tight_layout()

plt.savefig('cluster_by_io.svg')
