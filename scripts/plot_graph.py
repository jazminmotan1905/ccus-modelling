import numpy as np
import pandas as pd
import networkx as nx
import seaborn as sns
from matplotlib import pyplot as plt

fpath = "../csv_ccus/1.csv"

df=pd.read_csv(fpath, index_col=0)

# Quickly view dataframe
sns.heatmap(df)
plt.show()

# Make all values positive, showing the direction
mat = df.values
for x,y in zip(*np.where(df<0)):
    mat[x,y] = mat[y,x]
    mat[y,x] = np.nan
mat[mat==0] = 0.01
# mat[np.isnan(mat)] = 0
fixed_df = pd.DataFrame(abs(mat), index=df.index, columns=df.columns)


# g = nx.convert_matrix.from_pandas_adjacency(df.values, parallel_edges=True)
g = nx.from_numpy_matrix(fixed_df.notna().values, parallel_edges = True, create_using=nx.MultiDiGraph())
g = nx.relabel_nodes(g, {i:v for i,v in enumerate(fixed_df.index)})
sizes = fixed_df.sum(axis=1)
names, node_sizes = sizes.index, {k:v for k,v in sizes.items()}
nx.draw(g, node_size = node_sizes)
plt.show()
