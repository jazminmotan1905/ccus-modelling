import networkx as nx

# load graphs from data dir
graphs = {}
for i in range(1, 31):
    g = nx.readwrite.read_gpickle('data_v1/weighted_%s.gpickle' % i)
    g.name = i
    graphs[i] = g


def graph_from_cluster(cluster):
    G = nx.DiGraph()
    for p in cluster:
        for e in graphs[p].edges:
            edge_data = G.get_edge_data(*e)
            if edge_data is None:
                w = graphs[p].get_edge_data(*e)['weight']
            else:
                w = edge_data['weight'] + graphs[p].get_edge_data(*e)['weight']
        
            G.add_edge(*e, weight=w)
                 
    return G



CLUSTER_A = [3, 15, 2, 23, 5, 14, 26]
nx.readwrite.write_graphml(graph_from_cluster(CLUSTER_A),
                           'data_v1/cluster_a.graphml')
nx.readwrite.write_edgelist(graph_from_cluster(CLUSTER_A),
                           'data_v1/cluster_a.edgelist', data=True)
nx.readwrite.write_gpickle(graph_from_cluster(CLUSTER_A),
                           'data_v1/cluster_a.gpickle')


CLUSTER_B = [4, 12, 6, 1, 10, 17, 25, 9, 8, 16]
nx.readwrite.write_graphml(graph_from_cluster(CLUSTER_B),
                           'data_v1/cluster_b.graphml')
nx.readwrite.write_edgelist(graph_from_cluster(CLUSTER_B),
                           'data_v1/cluster_b.edgelist', data=True)
nx.readwrite.write_gpickle(graph_from_cluster(CLUSTER_B),
                           'data_v1/cluster_b.gpickle')


CLUSTER_C = [13, 11, 29, 20, 24, 7, 18, 22, 30, 21, 27, 19, 28]
nx.readwrite.write_graphml(graph_from_cluster(CLUSTER_C),
                           'data_v1/cluster_c.graphml')
nx.readwrite.write_edgelist(graph_from_cluster(CLUSTER_C),
                            'data_v1/cluster_c.edgelist', data=True)
nx.readwrite.write_gpickle(graph_from_cluster(CLUSTER_C),
                           'data_v1/cluster_c.gpickle')
