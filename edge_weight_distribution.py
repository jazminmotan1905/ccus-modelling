import networkx as nx
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

# load graphs from data dir
graphs = []
for i in range(1, 31):
    g = nx.readwrite.read_gpickle('data_v1/weighted_%s.gpickle' % i)
    g.name = i
    graphs.append(g)

# full set of edges
edges = set()
for g in graphs:
    edges = edges.union(set(g.edges))
edges = list(edges)

# edge is key, list of weights is the value
weights = {e: [] for e in edges}
for g in graphs:
    for e in g.edges:
        w = g.get_edge_data(*e)['weight']
        if w is not None:
            weights[e].append(w)

d = []
for e in weights:
    for w in weights[e]:
        d.append(['→'.join(e), w])

means = {e: np.mean(weights[e]) for e in weights}
variances = {e: np.var(weights[e]) for e in weights}

stats = pd.DataFrame({'edge': ['→'.join(e) for e in weights], 
                      'mean': means.values(),
                      'var': variances.values(),
                      'N': [len(weights[e]) for e in weights]})

stats.to_csv('edges_weight_dist_stats.csv')
