# SOURCES

## Assumptions

An emitter is any industrial source that generates GHG; their SIZE is define by the amount of CO2 tons emitted per year (data: INECC, 2017); the sources are distributed in 3 regions: North Gulf of Mexico (N-GoM), Central Gulf of Mexico (C-GoM) and Southeast (SE); 

```
If the emitter SIZE is greater than 750,001 tons of CO2
Then the emitter is an “anchor” 
	
If the emitter SIZE is less than 750,000 tons of CO2
Then the emitter is a “tie-in” if it is less than 20km from an “anchor” emitter 
	#Depends on the distribution of the emitters and the size
Then the OPPORTUNITY FOR CLUSTERING is likely
	If not the OPPORTUNITY FOR CLUSTERING is unlikely

# There is a relationship between TRL (IEA,2020), COST and ADOPTION LIKELIHOOD. Process described in the “Sources_Capture_Tecnology” chart (PDF).

If the SECTOR is CHEMICALS
  And the INDUSTRY is AMMONIA
     and the TECHNOLOGY is CHEMICAL ABSORPTION,
        then the TRL is MATURE
Therefore the COST is moderate and the ADOPTION LIKELIHOOD is very likely

If the SECTOR is CHEMICALS
  And the INDUSTRY is AMMONIA
     and the TECHNOLOGY is PHYSICAL ABSORPTION,
        then the TRL is EARLY ADOPTION
Therefore the COST is moderate and the ADOPTION LIKELIHOOD is most likely

If the SECTOR is CHEMICALS
  And the INDUSTRY is METHANOL
     and the TECHNOLOGY is CHEMICAL ABSORPTION,
        then the TRL is EARLY ADOPTION
Therefore the COST is moderate and the ADOPTION LIKELIHOOD is most likely

If the SECTOR is CHEMICALS
  And the INDUSTRY is METHANOL
     and the TECHNOLOGY is PHYSICAL ABSORPTION,
        then the TRL is DEMONSTRATION
Therefore the COST is high and the ADOPTION LIKELIHOOD is likely
```

