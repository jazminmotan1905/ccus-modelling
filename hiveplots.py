from itertools import permutations
import networkx as nx
import pyveplot
from pyveplot import Hiveplot, Node, Axis, dwg
import node_categories as cat
import random
import sys
import pathlib


def plot_graph(g):
    g = g.to_undirected()
    h = Hiveplot()

    # create three axes, spaced at 120 degrees from each other
    h.axes = [Axis(start=10, angle=0,
                   stroke=cat.c['technical'], stroke_width=1.1),
              Axis(start=20, angle=120,
                   stroke=cat.c['sociopolitical'], stroke_width=1.1),
              Axis(start=20, angle=120 * 2,
                   stroke=cat.c['sustainability'], stroke_width=1.1),
              Axis(start=125, angle=120 * 2,
                   stroke=cat.c['economic'], stroke_width=1.1)
    ]

    k = list(nx.degree(g))
    k.sort(key=lambda tup: tup[1])

    for axis, nodes, axname in zip(h.axes,
                                 [
                                     cat.technical,
                                     cat.sociopolitical,
                                     cat.sustainability,
                                     cat.economic,

                                 ],

                                    ['technical',
                                     'sociopolitical',
                                     'sustainability',
                                     'economic']):
        circle_color = cat.c[axname]

        for v in nodes:

            if v in g.nodes:
                node = Node(radius=g.degree(v), label=v)
            else:
                node = Node(radius=0, label=v)

            # add it to axis
            axis.add_node(v, node)

            # once it has x, y coordinates, add a circle
            node.add_circle(fill=circle_color, stroke=circle_color,
                            stroke_width=0.1, fill_opacity=0.7)


            # if axis.angle < 180:
            #     orientation = -1
            #     scale = 0.6
            # else:
            #     orientation = 1
            #     scale = 0.35
            # # also add a label
            # node.add_label("%s k=%s" % (v, g.degree(v)),
            #                angle=axis.angle + 90 * orientation,
            #                scale=scale)

        curve_color = '#78716c'
        h.connect_axes(h.axes[1], h.axes[2],
                       g.edges, stroke_width=0.5, stroke=curve_color)
        h.connect_axes(h.axes[1], h.axes[3],
                       g.edges, stroke_width=0.5, stroke=curve_color)

        h.connect_axes(h.axes[0], h.axes[1],
                       g.edges, stroke_width=0.5, stroke=curve_color)


        h.connect_axes(h.axes[2], h.axes[0],
                       g.edges, stroke_width=0.5, stroke=curve_color)


        h.connect_axes(h.axes[3], h.axes[0],
                       g.edges, stroke_width=0.5, stroke=curve_color)



    h.save('hiveplots/hiveplot_%s.svg' % path.stem)
    if len(g.name)>1:
        dwg.add(dwg.text(g.name, x=[-20, ],  y=[6, ], font_size=14))
    else:
        dwg.add(dwg.text(g.name, x=[-10, ],  y=[6, ], font_size=14))        
    dwg.saveas('hiveplots/hiveplot_%s.svg' % path.stem)
    del(h)


path = pathlib.Path(sys.argv[1])
g = nx.readwrite.read_gpickle(path.as_posix())
g.name = path.name.split('_')[1].replace('.gpickle', '')

plot_graph(g)
