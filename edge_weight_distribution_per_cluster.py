import networkx as nx
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

def get_graphs(cluster):
    """ load graphs for selected ids from data dir"""
    graphs = []
    for i in cluster:
        g = nx.readwrite.read_gpickle('data_v1/weighted_%s.gpickle' % i)
        g.name = i
        graphs.append(g)
    return graphs


def write_stats(cluster, tag):
    graphs = get_graphs(cluster)
    # full set of edges
    edges = set()
    for g in graphs:
        edges = edges.union(set(g.edges))
    edges = list(edges)

    # edge is key, list of weights is the value
    weights = {e: [] for e in edges}
    for g in graphs:
        for e in g.edges:
            w = g.get_edge_data(*e)['weight']
            if w is not None:
                weights[e].append(w)

    with open("data_v1/edge_weights_%s.csv" % tag, 'w') as f:
        for e in weights:
            f.write('→'.join(e) + ',')
            f.write(','.join([str(w) for w in weights[e]]) + '\n')

    d = []
    for e in weights:
        for w in weights[e]:
            d.append(['→'.join(e), w])



    means = {e: np.mean(weights[e]) for e in weights}
    variances = {e: np.var(weights[e]) for e in weights}

    stats = pd.DataFrame({'edge': ['→'.join(e) for e in weights],
                          'mean': means.values(),
                          'var': variances.values(),
                          'N': [len(weights[e]) for e in weights]})

    stats.to_csv('data_v1/edges_weight_dist_stats_%s.csv' % tag)



CLUSTER_A = [3, 15, 2, 23, 5, 14, 26]
CLUSTER_B = [4, 12, 6, 1, 10, 17, 25, 9, 8, 16]
CLUSTER_C = [13, 11, 29, 20, 24, 7, 18, 22, 30, 21, 27, 19, 28]

write_stats(CLUSTER_A, 'cluster_a')
write_stats(CLUSTER_B, 'cluster_b')
write_stats(CLUSTER_C, 'cluster_c')
