
pickles:
	python adjmatrixes2nxgpickles.py

edges_weight_dist_stats.csv: pickles
	python edge_weight_distribution.py

cluster_by_io.svg: pickles
	python cluster_by_io.py


clusters: pickles
	python nw_union_by_cluster.py
	python edge_weight_distribution_per_cluster.py


hiveplots: pickles
	find data_v1/ -iname '*gpickle' -exec python hiveplots.py {} \;

.PHONY: clusters hiveplots pickles

